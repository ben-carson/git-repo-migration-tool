#!/bin/bash

# no longer incrementing the tag for each migration,
# keeping this code in case its useful in the future
#function incrementbuildtag {
#	echo You have chosen to increment the tag.
	#strip off the 's'
#	sprint_num="$(echo $curr_tag | cut -d"s" -f2)"
	#strip off the build iteration to just get the sprint number
#	sprint_num="$(echo $sprint_num | cut -d. -f1)"
#	build_num="$(echo $curr_tag | cut -d. -f2)"
#	new_build_num=s$sprint_num.$((build_num+1))
#}

function incrementsprinttag {
	echo You have chosen to begin a new sprint.
	sprint_num="$(echo $curr_tag | cut -d. -f1)"
	#tag convention puts an 's' in front of the tag, let's get the number after that "s"
	sprint_num="$(echo $sprint_num | cut -d"s" -f2)"
	new_build_num=s$((sprint_num+1)).0
	while :
	do
		echo Tag $curr_tag will be bumped to $new_build_num.
		echo Please confirm this is correct. \(y/n\) 
		read tag_change_confirm
		case "$tag_change_confirm" in
		y) echo $new_build_num confirmed.
			break
			;;
		n) echo $new_build_num is incorrect. Resetting to $curr_tag.
			new_build_num=$curr_tag
			break
			;;
		*) echo "Invalid Selection!"
			echo ""
			;;
		esac
	done
}

function printcurrenttag {
	curr_tag="$(git tag | tail -n1)"
	echo The current tag is $curr_tag.
}

function printnewtag {
	echo new tag will be $new_build_num.
}

function createnewtag {
	
	while :
	do
#		echo "i: Increment the tag build number"
		echo "b: Create a bundle with the existing sprint tag"
		echo "n: Create a new sprint tag"
#		echo "q: exit this tool"
		echo "Select your option:"
		read next_tag_type
		echo "You have selected $next_tag_type."

		case "$next_tag_type" in
#		i) incrementbuildtag
#			break
#			;;
		b) #do nothing, curr_tag will remain the tag to use
			new_build_num=$curr_tag
			break
			;;
		n) incrementsprinttag
			break
			;;
#		q) exit 0
#			;;
		*) echo "Invalid Selection!"
			echo ""
			;;
		esac
	done
}

function addnewtagtorepo {
	git tag -f $new_build_num
	git push --tags
}
