#This is the upload portion of the offline git repo
#TODO: check if 'upstream' remote exists for each repo and throw error and exit if not
#if (fatal: '${upstream_remote}' does not appear to be a git repository) appears, then remote does not exist

#TODO: have user define their 'upstream' remote, $upstream_remote
upstream_remote=upstream

read -p "Provide project directory [/d/dev/projects]: " super_filepath
#TODO: insert a user confirmation here, to make sure the correct filepath was provided
cd ${super_filepath}

#TODO, provide a warning that EACH repository will have any uncommited changes removed and overwritten
printf "/n%s/n" updating each offline submodule repo

printf "/n%s/n" "--- Shared ---"
cd ./Assets/Shared
#TODO git restore or reset to match up with what's in the upstream
git checkout develop
git pull ${upstream_remote} develop --tags
git push origin develop --tags
printf "/n%s/n" "--- derahS ---"

printf "/n%s/n" "--- Global ---"
cd ../Global
#TODO git restore or reset to match up with what's in the upstream
git checkout develop
git pull ${upstream_remote} develop --tags
git push origin develop --tags
printf "/n%s/n" "--- labolG ---"

printf "/n%s/n" "--- Third Party ---"
cd ../Third\ Party
#TODO git restore or reset to match up with what's in the upstream
git checkout develop
git pull ${upstream_remote} develop --tags
git push origin develop --tags
printf "/n%s/n" "--- ytraP drihT ---"

printf "/n%s/n" "--- Super ---"
cd ../..
#TODO git restore or reset to match up with what's in the upstream
git checkout develop
git pull ${upstream_remote} develop --tags
git push origin develop --tags
printf "/n%s/n" "--- repuS ---"

