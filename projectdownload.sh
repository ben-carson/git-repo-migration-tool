#!/bin/bash
source ./latesttag.sh

#first find the location of the project
read -p "Provide project directory [/c/dev/projects]: " project_dir
project_dir=${project_dir:-/c/dev/projects}
printf "%s\n\n" "Provided project directory is $project_dir"

read -p "Provide sneakernet device directory [/e/dev/artifacts]: " artifact_dir
artifact_dir=${artifact_dir:-/e/dev/artifacts}
printf "%s\n" "Provided artifact directory is $artifact_dir"

cd $project_dir
#taken from
# https://stackoverflow.com/questions/1371261/get-current-directory-name-without-full-path-in-a-bash-script
printf "%s\n\n" "Moving to project directory: ${PWD##*/}"

printcurrenttag
echo ""
createnewtag
echo ""

printf "\n%s\n" "--- Shared ---"

cd ./Assets/Shared
current_folder_name=${PWD##*/}
git checkout develop
git pull origin develop

#create new tag here
if [[ "$curr_tag" != "$new_build_num" ]]
then
	addnewtagtorepo
fi
#echo "git bundle create $artifact_dir/shared.bundle $curr_tag..develop --tags"

git bundle create "$artifact_dir/$current_folder_name.bundle" $curr_tag..develop --tags

printf "\n%s\n" "--- derahS ---"

printf "\n%s\n" "--- Global ---"
cd ../Global
current_folder_name="${PWD##*/}"
git checkout develop
git pull origin develop
if [[ "$curr_tag" != "$new_build_num" ]]
then
	addnewtagtorepo
fi

git bundle create "$artifact_dir/$current_folder_name.bundle" $curr_tag..develop --tags

printf "\n%s\n" "--- labolG ---"

printf "\n%s\n" "--- Third Party ---"

cd ../Third\ Party
current_folder_name="${PWD##*/}"
git checkout develop
git pull origin develop
#somehow, if no updates are detected and pulled here, need to ignore the rest of the block here
if [[ "$curr_tag" != "$new_build_num" ]]
then
	addnewtagtorepo
fi

#because Third Party doesn't get updated very often, the sprintly migration doesn't make sense
#just bundle up the entire repo ("develop" branch) for migration
git bundle create "$artifact_dir/$current_folder_name.bundle" develop --tags

printf "\n%s\n" "--- ytraP drihT ---"

printf "\n%s\n" "--- Super ---"

cd ../..
current_folder_name="${PWD##*/}"
git checkout develop
git pull origin develop

printf "\n"
if [[ "$curr_tag" != "$new_build_num" ]]
then
	printnewtag
	printf "\n"
	addnewtagtorepo
fi

git bundle create "$artifact_dir/$current_folder_name.bundle" $curr_tag..develop --tags

printf "\n%s\n" "--- repuS ---"

